﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace MofoBot.Elements.Sessions
{
    /// <summary>
    /// Interaction logic for View.xaml
    /// </summary>
    public partial class View : UserControl
    {

        public Dictionary<Bot.BotStatus, string> StatusColors = new Dictionary<Bot.BotStatus, string>() { 
            {Bot.BotStatus.Paused,"#ff0000"},
            {Bot.BotStatus.Shotting,"#66ff00"},
            {Bot.BotStatus.Starting,"#ffff00"},
            {Bot.BotStatus.Waiting,"#0000ff"}
        };


        public static readonly DependencyProperty SessionProperty =
DependencyProperty.Register("Session", typeof(MofoBot.Bot.Session), typeof(View));
        public MofoBot.Bot.Session Session
        {
            get
            {
                return this.GetValue(SessionProperty) as MofoBot.Bot.Session;
            }
            set
            {
                this.SetValue(SessionProperty, value);
            }
        }

        public View()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            VBotStatus.ItemsSource = StatusColors;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new Elements.HtmlViewer(Session.HTML).ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ShowEditForm(this.Session.BaseName);
        }
        private void ShowEditForm(string Base)
        {
            try
            {
                //App.Log("MofoBot.Bot." + Base + ".Forms.Sessions.Shot");
                Type type = Type.GetType("MofoBot.Bot." + Base + ".Forms.Sessions.Edit");
                Window ctl = (Window)Activator.CreateInstance(type, this.Session);
                Window instance = ctl;
                instance.ShowDialog();
            }
            catch (Exception)
            {

            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (Session.Kill != null)
            {
                Session.Kill.Invoke();
                Session.Kill = null;
                Session.BotStatus = Bot.BotStatus.Waiting;
            }
            Session.InUse = false;
            Session.Wrapper.ReloadSessions();
            Bot.Settings.SessionsInUse.Remove(Session);
        }

        private void PauseSending_Click(object sender, RoutedEventArgs e)
        {
            Session.BotStatus = Bot.BotStatus.Paused;
            PauseSending.Visibility = System.Windows.Visibility.Collapsed;
            StartSending.Visibility = System.Windows.Visibility.Visible;
        }

        private void StartSending_Click(object sender, RoutedEventArgs e)
        {
            Session.BotStatus = Bot.BotStatus.Shotting;
            PauseSending.Visibility = System.Windows.Visibility.Visible;
            StartSending.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}