﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MofoBot.Elements.Sessions
{
    /// <summary>
    /// Interaction logic for Add.xaml
    /// </summary>
    public partial class Add : Window
    {
        public Add()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Bot.Settings.Bases.ToList().ForEach(item =>
            {
                TabItem page = new TabItem();
                page.Header = item.Key;
                page.Content = GetAddForm(item.Key);
                Forms.Items.Add(page);
            });
        }
        private StackPanel GetAddForm(string Base)
        {
            try
            {
                Console.WriteLine("MofoBot.Bot." + Base + ".Forms.Sessions.Add");
                Type type = Type.GetType("MofoBot.Bot." + Base + ".Forms.Sessions.Add");
                Control ctl = (Control)Activator.CreateInstance(type);
                StackPanel instance = new StackPanel();
                instance.Children.Add(ctl);
                return instance;
            }
            catch (Exception)
            {
                return new StackPanel();
            }
        }
    }
}
