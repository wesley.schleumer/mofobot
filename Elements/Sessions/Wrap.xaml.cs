﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace MofoBot.Elements.Sessions
{
    /// <summary>
    /// Interaction logic for Wrap.xaml
    /// </summary>
    public partial class Wrap : UserControl
    {


        public static readonly DependencyProperty SessionProperty =
DependencyProperty.Register("Session", typeof(MofoBot.Bot.Session), typeof(Wrap));
        public MofoBot.Bot.Session Session
        {
            get
            {
                return this.GetValue(SessionProperty) as MofoBot.Bot.Session;
            }
            set
            {
                this.SetValue(SessionProperty, value);
            }
        }

        public Wrap()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Conteudo.Children.Add(GetShotForm(Session.BaseName));
                BitmapImage img = new BitmapImage(new Uri(Directory.GetCurrentDirectory()+"\\Assets\\Logos\\" + Session.BaseName + ".png"));
                Logo.Source = img;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        private StackPanel GetShotForm(string Base)
        {
            try
            {
                //App.Log("MofoBot.Bot." + Base + ".Forms.Sessions.Shot");
                Type type = Type.GetType("MofoBot.Bot." + Base + ".Forms.Sessions.Shot");
                Control ctl = (Control)Activator.CreateInstance(type,this.Session);
                StackPanel instance = new StackPanel();
                instance.Children.Add(ctl);
                return instance;
            }
            catch (Exception)
            {
                return new StackPanel();
            }
        }
    }
}
