﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace MofoBot.Elements.Sessions
{
    /// <summary>
    /// Interaction logic for Status.xaml
    /// </summary>
    public partial class Status : UserControl
    {
        public static readonly DependencyProperty StatusProperty = DependencyProperty.Register("BotStatus", typeof(Bot.BotStatus), typeof(Status));
        public Bot.BotStatus BotStatus
        {
            get
            {
                return (Bot.BotStatus)this.GetValue(StatusProperty);
            }
            set
            {
                this.SetValue(StatusProperty, value);
            }
        }

        public Status()
        {
            Reload();
            InitializeComponent();
            Reload();
        }

        public void Reload()
        {
            switch (BotStatus)
            {
                case Bot.BotStatus.Paused:
                    this.Background = Brushes.Red;
                    break;
                case Bot.BotStatus.Shotting:
                    this.Background = Brushes.Green;
                    break;
                case Bot.BotStatus.Starting:
                    this.Background = Brushes.Yellow;
                    break;
                case Bot.BotStatus.Waiting:
                    this.Background = Brushes.Blue;
                    break;
            }
        }
    }
}
