﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MofoBot.Elements
{
    /// <summary>
    /// Interaction logic for NicknameManagement.xaml
    /// </summary>
    public partial class NicknameManagement : Window
    {
        public NicknameManagement()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Nicknames.ItemsSource = Bot.Settings.Nicknames;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddNickname();
        }

        private void Nickname_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AddNickname();
            }
        }

        private void AddNickname()
        {
            Bot.Nickname nick = new Bot.Nickname();
            nick.Value = Nickname.Text;
            Bot.Settings.Nicknames.Add(nick);
            Nickname.Text = String.Empty;
            Nickname.Focus();
        }
    }
}
