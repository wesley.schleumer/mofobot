﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MofoBot.Elements
{
    /// <summary>
    /// Interaction logic for ShotSessions.xaml
    /// </summary>
    public partial class ShotSessions : UserControl
    {
        public ShotSessions()
        {
            InitializeComponent();
        }
        public void ReloadSessions(bool useFocus = true)
        {
            try
            {
                var x = (from i in Bot.Settings.Sessions where i.InUse == false select i).Take(8);
                int focusIt = 0;
                foreach (var y in x)
                {
                    if (focusIt == 0 && useFocus)
                    {
                        y.FocusIt = true;
                    }
                    else
                    {
                        y.FocusIt = false;
                    }
                    y.Wrapper = this;
                    focusIt++;
                }
                SessionsDisplay.DataContext = x;
            }
            catch (Exception) { }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ReloadSessions();
            //Bot.Settings.Sessions.ListChanged += new System.ComponentModel.ListChangedEventHandler((x, s) =>
            //{
            //    ReloadSessions(false);
            //});
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //this.Hide();
            e.Cancel = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReloadSessions();
        }
    }
}
