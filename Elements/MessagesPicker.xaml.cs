﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MofoBot.Elements
{
    /// <summary>
    /// Interaction logic for MessagesPicker.xaml
    /// </summary>
    public partial class MessagesPicker : Window
    {
        Bot.Messages messages = new Bot.Messages();
        public MessagesPicker()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            messageList.ItemsSource = messages;
            messageList.DisplayMemberPath = "Text";
        }

        private void Add(string text)
        {
            if (text.Length > 0)
            {
                messages.Add(new Bot.Message()
                {
                    Text = text
                });
            }
            else
            {
                MessageBox.Show("Preencha o texto");
            }
        }

        private void newMessage_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Add(newMessage.Text);
                newMessage.Text = "";
            }
        }

        private void messageList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                if (messageList.SelectedItems.Count > 0)
                {
                    messages.Remove((Bot.Message)messageList.SelectedItems[0]);
                }
            }
        }

    }
}
