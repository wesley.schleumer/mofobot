﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MofoBot.Elements
{
    /// <summary>
    /// Interaction logic for SessionManagement.xaml
    /// </summary>
    public partial class SessionManagement : Window
    {
        public SessionManagement()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dataGrid1.ItemsSource = Bot.Settings.Sessions;
            if (Bot.Settings.Sessions.Count > 0) dataGrid1.SelectedIndex = 0;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            new Elements.Sessions.Add().ShowDialog();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid1.SelectedItems.Count > 0)
                {
                    //App.Log("MofoBot.Bot." + Base + ".Forms.Sessions.Shot");
                    Bot.Session sess = (Bot.Session)dataGrid1.SelectedItem;
                    Type type = Type.GetType("MofoBot.Bot." + sess.BaseName + ".Forms.Sessions.Edit");
                    Window ctl = (Window)Activator.CreateInstance(type, sess);
                    Window instance = ctl;
                    instance.ShowDialog();
                }
            }
            catch (Exception)
            {

            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedItems.Count > 0)
            {
                if (MessageBox.Show("Você realmente deseja remover essa Sessão?", "Sistema", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    Bot.Session sess = (Bot.Session)dataGrid1.SelectedItem;
                    Bot.Settings.Sessions.Remove(sess);
                }
            }
        }
    }
}
