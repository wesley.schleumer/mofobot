﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace MofoBot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Elements.ShotSessions ShotSessions = new Elements.ShotSessions();
        public MainWindow()
        {
            //new Elements.MessagesPicker().ShowDialog();
            //Process.GetCurrentProcess().Kill();
            //new Window()
            //{
            //    Content = new Bot.Uol.Forms.Sessions.Add(),
            //    WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen
            //}.ShowDialog();
            //Process.GetCurrentProcess().Kill();
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            new Elements.SessionManagement().ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SessionsDisplay.DataContext = Bot.Settings.SessionsInUse;

            SendMessageSlider.DataContext = Bot.Settings.MoreSettings;
            SendPrivateMessageSlider.DataContext = Bot.Settings.MoreSettings;

            SendMessageTimer.DataContext = Bot.Settings.MoreSettings;
            SendPrivateMessageTimer.DataContext = Bot.Settings.MoreSettings;

            SendMessageSlider.Value = 5;
            SendPrivateMessageSlider.Value = 300;

            Utils.Logger.App.Logger = this.textBox1;
            Utils.Logger.App.LoggerScrollViewer = this.scrollViewer1;
            Utils.Logger.Write("Programa Iniciado");

            Bot.Settings.MessageQueue.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler((ss,ee) => {
                if (ee.PropertyName == "QueueCount")
                {
                    this.Dispatcher.BeginInvoke(new Action(() => {
                        Estabilidade.Header = String.Format("Fila de Requisição: {0}", Bot.Settings.MessageQueue.QueueCount);
                    }));
                }
            });

            new Thread(() => {
                PerformanceCounter cpuCounter;
                PerformanceCounter ramCounter;

                cpuCounter = new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName);

                ramCounter = new PerformanceCounter("Process", "Working Set", Process.GetCurrentProcess().ProcessName);

                do
                {
                    string str = String.Format("Uso de memoria: {0}MB, Uso do Processador: {1}%", Math.Round((ramCounter.NextValue() / 1024 / 1024)), Math.Round(cpuCounter.NextValue()));
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Processo.Header = str;
                    }));
                    Thread.Sleep(1000);
                } while (true);
            }).Start();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //ShotSessions.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

        private void SendMessageSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Bot.Settings.MoreSettings.SendMessageInterval = ((Slider)sender).Value;
        }

        private void SendPrivateMessageSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Bot.Settings.MoreSettings.SendPrivateMessageInterval = ((Slider)sender).Value;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            new Elements.NicknameManagement().Show();
        }
    }
}
