﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.IO;
using System.Web;
using System.Windows;

namespace MofoBot.Utils
{
    public enum HttpRequestMethods
    {
        GET = 0,
        POST = 1
    }
    public delegate void HttpRequestComplete(string content);
    public delegate void HttpRequestProgress(string content);
    public delegate void ResponseInfoDelegate(string statusDescr, string contentLength);
    public delegate void ProgressDelegate(string piece, string content);
    public delegate void AsyncProgressDelegate(AsyncState state);
    public delegate void KillAsyncDelegate();
    public delegate void DoneDelegate(string content);
    public class HTTP
    {
        const int BUFFER_SIZE = 1024;
        public string ReponseFileName = Bot.Settings.TempDir + "HTTP.FS." + Guid.NewGuid().ToString();

        public event HttpRequestComplete HttpRequestComplete;
        public event ResponseInfoDelegate Response;
        public event ProgressDelegate Progress;
        public event DoneDelegate Done;

        private CookieContainer _cookieJar = new CookieContainer();

        public CookieContainer CookieJar
        {
            get { return _cookieJar; }
            set { _cookieJar = value; }
        }

        private HttpRequestMethods _method;

        public HttpRequestMethods Method
        {
            get { return _method; }
            set { _method = value; }
        }

        private Encoding _postEncoding = Encoding.UTF8;

        public Encoding PostEncoding
        {
            get { return _postEncoding; }
            set { _postEncoding = value; }
        }
        

        public HTTP()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                   delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
                   {
                       return true; // **** Always accept
                   };
        }

        private string[] _methods = new string[] { "GET", "POST" };
        public HTTP RequestAsync(string url, Dictionary<string, string> postData = null)
        {
            new Thread(() =>
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.CookieContainer = CookieJar;
                    request.Method = _methods[System.Convert.ToInt32(Method)];
                    request.Proxy = null;
                    request.Accept = "ISO-8859-1,utf-8;q=0.7,*;q=0.3";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.92 Safari/537.4";
                    Stream dataStream;
                    if (Method == HttpRequestMethods.POST)
                    {
                        request.ContentType = "application/x-www-form-urlencoded";
                        string query = String.Empty;
                        if (postData != null)
                        {

                            List<string> parameters = new List<string>();

                            foreach (var key in postData.Keys)
                            {
                                parameters.Add(String.Format("{0}={1}",
                                    HttpUtility.UrlEncode(key),
                                    HttpUtility.UrlEncode(postData[key], Encoding.UTF8)));
                            }

                            query = String.Join("&", parameters);
                        }
                        request.BeginGetRequestStream(new AsyncCallback(delegate(IAsyncResult result)
                        {
                            HttpWebRequest inRequest = (HttpWebRequest)result.AsyncState;
                            Stream postStream = inRequest.EndGetRequestStream(result);
                            byte[] byteArray = Encoding.UTF8.GetBytes(query);
                            postStream.Write(byteArray, 0, query.Length);
                            postStream.Close();

                            inRequest.BeginGetResponse(new AsyncCallback
                                    (
                                    delegate(IAsyncResult responseResult)
                                    {
                                        HttpWebResponse resp = (HttpWebResponse)inRequest.EndGetResponse(responseResult);
                                        //StreamReader loResponseStream = new StreamReader(resp.GetResponseStream(), Encoding.GetEncoding(resp.CharacterSet.Length > 0 ? resp.CharacterSet : "UTF-8"));
                                        FileStream fs = new FileStream(this.ReponseFileName, FileMode.CreateNew);
                                        ReadWriteStream(resp.GetResponseStream(), fs);
                                        if (HttpRequestComplete != null)
                                        {
                                            HttpRequestComplete(File.ReadAllText(this.ReponseFileName));
                                        }

                                    }
                                    ), null);
                        }), request);
                    }
                    else
                    {
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        dataStream = response.GetResponseStream();
                        FileStream fs = new FileStream(this.ReponseFileName, FileMode.CreateNew);
                        ReadWriteStream(response.GetResponseStream(), fs);
                        if (HttpRequestComplete != null)
                        {
                            HttpRequestComplete(File.ReadAllText(this.ReponseFileName));
                        }

                        //request.BeginGetResponse(new AsyncCallback
                        //            (
                        //            delegate(IAsyncResult responseResult)
                        //            {
                        //                HttpWebResponse resp = (HttpWebResponse)request.EndGetResponse(responseResult);
                        //                //StreamReader loResponseStream = new StreamReader(resp.GetResponseStream(), Encoding.GetEncoding(resp.CharacterSet.Length > 0 ? resp.CharacterSet : "UTF-8"));
                        //                FileStream fs = new FileStream(this.ReponseFileName, FileMode.CreateNew);
                        //                ReadWriteStream(resp.GetResponseStream(), fs);
                        //                if (HttpRequestComplete != null)
                        //                {
                        //                    HttpRequestComplete(File.ReadAllText(this.ReponseFileName));
                        //                }

                        //            }
                        //            ), null);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }) { 
                Name = url
            }.Start();
            return this;
        }
        private void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

        public HTTP BindHttpRequestComplete(HttpRequestComplete method)
        {
            HttpRequestComplete += method;
            return this;
        }

        public void KillAsync()
        {
            if (KillThis != null)
            {
                KillThis();
            }
        }
        public event KillAsyncDelegate KillThis;
        public HTTP Async(string url, AsyncProgressDelegate progress = null)
        {
            new Thread(() =>
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = _methods[System.Convert.ToInt32(Method)];
                request.CookieContainer = CookieJar;
                request.Proxy = null;
                request.Accept = "ISO-8859-1,utf-8;q=0.7,*;q=0.3";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                AsyncState state = new AsyncState();

                byte[] buffer = new byte[255];
                int bytesRead;
                double totalBytesRead = 0;
                using (Stream responseStream = response.GetResponseStream())
                {
                    KillThis += new KillAsyncDelegate(() =>
                    {
                        new Thread(() =>
                        {
                            response.Close();
                        }).Start();
                    });
                    try
                    {
                        while ((bytesRead = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            totalBytesRead += bytesRead;
                            state.Stream.Write(buffer, 0, bytesRead);
                            if (progress != null)
                            {
                                progress(state);
                            }
                        }
                    }
                    catch (Exception) { }
                }

            }) { 
                Name = url
            }.Start();
            return this;
        }

        public String SimpleRequest(string url, Dictionary<string, string> data = null)
        {
            string requestContent = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = CookieJar;
            request.Method = _methods[System.Convert.ToInt32(Method)];
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.92 Safari/537.4";
            request.Proxy = null;
            request.Accept = "ISO-8859-1,utf-8;q=0.7,*;q=0.3";
            Stream dataStream;
            if (Method == HttpRequestMethods.POST)
            {
                request.ContentType = "application/x-www-form-urlencoded";
                string query = String.Empty;
                if (data != null)
                {

                    List<string> parameters = new List<string>();

                    foreach (var key in data.Keys)
                    {
                        parameters.Add(String.Format("{0}={1}",
                            HttpUtility.UrlEncode(key, PostEncoding),
                            HttpUtility.UrlEncode(data[key], PostEncoding)));
                    }

                    query = String.Join("&", parameters);
                }
                byte[] byteArray = PostEncoding.GetBytes(query);
                request.ContentLength = byteArray.Length;
                dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
            }
            WebResponse response = request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            requestContent = reader.ReadToEnd();
            return requestContent;
        }
    }

    public class AsyncState
    {
        public string Block = String.Empty;
        public Encoding Encoding = Encoding.UTF8;
        private string _content;

        public string Content
        {
            get
            {
                _content = Encoding.GetString(Stream.GetBuffer());
                return _content;
            }
            set { _content = value; }
        }


        public MemoryStream Stream = new MemoryStream();
        public AsyncState()
        {

        }
    }

}
