﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Collections;
using System.ComponentModel;

namespace MofoBot.Utils
{
    class TQueue : INotifyPropertyChanged
    {
        public static Dictionary<string,List<TQueueObject>> Queues = new Dictionary<string,List<TQueueObject>>();

        private int _queueCount;

        public int QueueCount
        {
            get { return _queueCount; }
            set { _queueCount = value; NotifyPropertyChanged("QueueCount"); }
        }
        

        private bool Playing = false;
        public string QueueID = Guid.NewGuid().ToString();
        public TQueue()
        {
            Utils.TQueue.Queues.Add(QueueID, new List<TQueueObject>());
        }
        public void Enqueue(TQueueObject obj)
        {
            List<TQueueObject> Threads = Utils.TQueue.Queues[QueueID];
            Threads.Add(obj);
            QueueCount = Threads.Count;
        }
        public void Play()
        {
            List<TQueueObject> Threads = Utils.TQueue.Queues[QueueID];
            if (Playing)
            {
                return;
            }
            Playing = true;
            foreach (var t in Threads)
            {
                t.TQueue = this;
                t.Thread.Start(t);
                return;
            }
        }
        public void Next()
        {
            Playing = false;
            List<TQueueObject> Threads = Utils.TQueue.Queues[QueueID];
            if (Threads.Count > 0)
            {
                Threads.Remove(Threads[0]);
                if (Threads.Count > 0)
                {
                    Play();
                }
                QueueCount = Threads.Count;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}

