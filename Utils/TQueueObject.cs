﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MofoBot.Utils
{
    class TQueueObject
    {
        private dynamic[] _args;

        public dynamic[] Arguments
        {
            get { return _args; }
            set { _args = value; }
        }

        private TQueue _tQueue;

        public TQueue TQueue
        {
            get { return _tQueue; }
            set { _tQueue = value; }
        }
        

        private Thread _thread;

        public Thread Thread
        {
            get { return _thread; }
            set { _thread = value; }
        }
        

    }
}
