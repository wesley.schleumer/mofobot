﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Threading;

namespace MofoBot
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public System.Windows.Controls.TextBox Logger;
        public System.Windows.Controls.ScrollViewer LoggerScrollViewer = null;
        public App()
        {
            Utils.Logger.App = this;
        }
        public void Log(object obj){
            new Thread(() => {
                do
                {
                    System.GC.Collect();
                    Thread.Sleep(1000);
                } while (true);
            }).Start();
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (Logger != null)
                {
                    Logger.AppendText(DateTime.Now.ToString("[dd/MM/yyyy HH:mm:ss] ") + obj.ToString() + "\n");
                    if (LoggerScrollViewer != null)
                    {
                        LoggerScrollViewer.ScrollToEnd();
                    }
                }
            }));
        }
    }
}