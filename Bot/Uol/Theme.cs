﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot.Uol
{
    class Theme : INotifyPropertyChanged
    {
        private long? _id;

        public long? Id
        {
            get { return _id; }
            set { _id = value; }
        }
        

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _url;

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        private Rooms _rooms;

        public Rooms Rooms
        {
            get { return _rooms; }
            set { _rooms = value; }
        }

        public void LoadRooms()
        {
            this.Rooms = new Rooms(this.Id.ToString());
        }
        
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
