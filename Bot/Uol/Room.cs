﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot.Uol
{
    public class Room : INotifyPropertyChanged
    {
        private long? _id;
        [Browsable(false)]
        public long? Id
        {
            get { return _id; }
            set { _id = value; NotifyPropertyChanged("Id"); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyPropertyChanged("Name"); }
        }

        private string _url;
        [Browsable(false)]
        public string Url
        {
            get { return _url; }
            set { _url = value; NotifyPropertyChanged("Url"); }
        }

        private long? _themeId;
        [Browsable(false)]
        public long? ThemeId
        {
            get { return _themeId; }
            set { _themeId = value; NotifyPropertyChanged("ThemeId"); }
        }

        private int _online;
        [DisplayName("Usuários Online")]
        public int Online
        {
            get { return _online; }
            set { _online = value; NotifyPropertyChanged("Online"); }
        }
        
        
        
        

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
