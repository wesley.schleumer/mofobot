﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CsQuery;

namespace MofoBot.Bot.Uol.Forms
{
    /// <summary>
    /// Interaction logic for RoomPicker.xaml
    /// </summary>
    public partial class RoomPicker : Window
    {
        public RoomPicker()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            themes.ItemsSource = Bot.Uol.Base.Themes.Where(item => item.Name.Contains(search.Text));
            themes.DisplayMemberPath = "Name";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SearchThis();
        }

        private void SearchThis()
        {
            themes.ItemsSource = Bot.Uol.Base.Themes.Where(item => item.Name.ToUpper().Contains(search.Text.ToUpper()));
        }

        private void search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchThis();
            }
        }

        private void themes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (themes.SelectedItems.Count < 1)
            {
                return;
            }
            this.IsEnabled = false;
            Theme theme = (Theme)themes.SelectedItem;
            theme.LoadRooms();
            Utils.HTTP http = new Utils.HTTP();
            http.Method = Utils.HttpRequestMethods.GET;
            http.HttpRequestComplete += new Utils.HttpRequestComplete((str) =>
            {
                var dom = CQ.Create(str);
                dom.Select(".tableListaSalas tr").Each((i, obj) =>
                {
                    string usersOnline = CQ.Create(obj).Select("td:last-child").First().Html();
                    string url = CQ.Create(obj).Select("a").First().Attr("href");
                    Rooms rs = theme.Rooms;
                    if (url != null)
                    {
                        (from room in rs where room.Url.Contains(url) select room).ToList().ForEach(item =>
                        {
                            item.Online = System.Convert.ToInt32(usersOnline);
                        });
                    }
                });
                this.Dispatcher.BeginInvoke(new Action(() => {
                    this.IsEnabled = true;
                    rooms.ItemsSource = theme.Rooms;
                }));
            });
            http.RequestAsync(theme.Url);

            //if (rooms.Rows.Count > 0)
            //{
            //    rooms.Rows[0].Selected = true;
            //}
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (rooms.SelectedItems.Count > 0)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("Escolha um sala.");
            }
        }
        new public Bot.Uol.Room ShowDialog()
        {
            base.ShowDialog();
            if (rooms.SelectedItems.Count > 0)
            {
                return (Bot.Uol.Room)rooms.SelectedItem;
            }
            else
            {
                return null;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            rooms.SelectedIndex = -1;
            this.Close();
        }
    }
}
