﻿using System.Windows;
using System.Windows.Controls;

namespace MofoBot.Bot.Uol.Forms.Sessions
{
    /// <summary>
    /// Interaction logic for Add.xaml
    /// </summary>
    public partial class Add : UserControl
    {
        public Bot.Session Session = new Session()
        {
            Color = "#000000",
            BaseName = "Uol",
            Method = "random"
        };
        private Bot.Uol.Room Room = null;
        public Add()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            sessionName.Focus();
            this.DataContext = Session;
            nicknames.ItemsSource = Settings.Nicknames;
            colors.ItemsSource = Bot.Uol.Base.Colors;
        }

        private void room_Click(object sender, RoutedEventArgs e)
        {
            Bot.Uol.Forms.RoomPicker picker = new Bot.Uol.Forms.RoomPicker();
            Room = picker.ShowDialog();
            if (Room != null)
            {
                Session.Name = Room.Name;
                Session.Url = Room.Url;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Session.Validate())
            {
                var name = Session.Name;
                Bot.Settings.Sessions.Add(Session);
                this.Session = new Session()
                {
                    Color = "#000000",
                    BaseName = "Uol",
                    Method = "random"
                };
                this.DataContext = this.Session;
                sessionName.Focus();
                sessionAddFooter.Text = "Sessão \""+name+"\" Adicionada com Sucesso";
            }
            else
            {
                MessageBox.Show("Ha campos não preenchidos");
            }
        }
    }
}
