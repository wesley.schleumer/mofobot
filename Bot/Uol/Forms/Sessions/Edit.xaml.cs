﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MofoBot.Bot.Uol.Forms.Sessions
{
    /// <summary>
    /// Interaction logic for Edit.xaml
    /// </summary>
    public partial class Edit : Window
    {
        public Bot.Session Session;
        private Bot.Uol.Room Room = null;

        public Edit(Bot.Session session)
        {
            this.Session = session;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = Session;
            nicknames.ItemsSource = Settings.Nicknames;
            colors.ItemsSource = Bot.Uol.Base.Colors;
        }
        


        private void room_Click(object sender, RoutedEventArgs e)
        {
            Bot.Uol.Forms.RoomPicker picker = new Bot.Uol.Forms.RoomPicker();
            Room = picker.ShowDialog();
            if (Room != null)
            {
                Session.Name = Room.Name;
                Session.Url = Room.Url;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Session.Validate())
            {
                var name = Session.Name;
                Bot.Settings.Sessions.Add(Session);
                this.Session = new Session()
                {
                    Color = "#000000",
                    BaseName = "Uol"
                };
                this.DataContext = this.Session;
                sessionName.Focus();
                sessionAddFooter.Text = "Sessão \""+name+"\" Adicionada com Sucesso";
            }
            else
            {
                MessageBox.Show("Ha campos não preenchidos");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Session.Save();
        }
    }
}
