﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsQuery;
using System.Threading;
using System.Text.RegularExpressions;
using MofoBot.Utils;

namespace MofoBot.Bot.Uol.Forms.Sessions
{
    /// <summary>
    /// Interaction logic for Shot.xaml
    /// </summary>

    public delegate void CheckRoomCode(string content);
    public delegate void CheckUsersOnline(string content);
    public partial class Shot : UserControl
    {

        private Thread SendThread = null;
        private Thread SendPrivateThread = null;

        public Bot.Session Session;
        public string CaptchaImageFile = String.Empty;
        public Dictionary<string, string> CaptchaPostData;
        public bool CaptchaLoaded = false;
        public string ListemURL = String.Empty;
        public string SendURL = String.Empty;
        public string SendQueryString = String.Empty;

        public Shot(Bot.Session Session)
        {
            this.Session = Session;
            InitializeComponent();
        }

        public event CheckRoomCode CheckRoomCode;
        public event CheckUsersOnline CheckUsersOnline;

        private bool SendingPrivate = false;

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            this.DataContext = Session;
            if (Session.FocusIt)
            {
                captchaReceiver.Focus();
            }
        }

        private void GetCaptchaImage()
        {
            if (!CaptchaLoaded)
            {
                imageHolder.Source = null;
                CaptchaLoaded = true;
                shotStatus.Content = "Buscando Imagem...";

                Utils.HTTP http = new Utils.HTTP()
                {
                    CookieJar = Session.CookieJar,
                    Method = Utils.HttpRequestMethods.GET
                };
                http.HttpRequestComplete += new Utils.HttpRequestComplete((str) =>
                {
                    CsQuery.CQ html = new CQ(str);
                    var form = html.Find("form[name=formnick]");
                    if (Session.Base.Logged)
                    {
                        CaptchaPostData = new Dictionary<string, string>()
                    {
                        {"nodeid",form.Find("[name=nodeid]").Val()},
                        {"mode",form.Find("[name=mode]").Val()},
                        {"th",form.Find("[name=th]").Val()},
                        {"typePage",form.Find("[name=typePage]").Val()},
                        {"co",Session.Color.ToLower()},
                        {"key",form.Find("[name=key]").Val()},
                        {"text",""},
                        {"ni",Session.Nickname},
                        {"corA",form.Find("[name=corA]").Val()},
                        {"ic",form.Find("[name=ic]").Val()},
                        {"ich",form.Find("[name=ich]").Val()},
                        {"ioc","y"}
                    };
                        if (!Session.Base.UseProfile)
                        {
                            CaptchaPostData["ioc"] = "n";
                        }
                    }
                    else
                    {
                        CaptchaPostData = new Dictionary<string, string>()
                    {
                        {"nodeid",form.Find("[name=nodeid]").Val()},
                        {"mode",form.Find("[name=mode]").Val()},
                        {"th",form.Find("[name=th]").Val()},
                        {"typePage",form.Find("[name=typePage]").Val()},
                        {"co",Session.Color.ToLower()},
                        {"key",form.Find("[name=key]").Val()},
                        {"text",""},
                        {"ni",Session.Nickname},
                        {"corA",form.Find("[name=corA]").Val()}
                    };
                    }

                    string imageUrl = html.Find(".testTuring img").First().Attr("src");

                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        shotStatus.Content = "Carregando Imagem...";
                    }));

                    Utils.HTTP imageGetter = new Utils.HTTP()
                    {
                        CookieJar = Session.CookieJar,
                        Method = Utils.HttpRequestMethods.GET
                    };
                    imageGetter.HttpRequestComplete += new Utils.HttpRequestComplete((imageStr) =>
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            imageHolder.Source = new BitmapImage(new Uri(imageGetter.ReponseFileName));
                        }));
                    });
                    imageGetter.RequestAsync(imageUrl);
                });
                http.RequestAsync(Session.Url);
            }
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            GetCaptchaImage();
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (CaptchaLoaded)
            {
                CaptchaLoaded = false;
            }
            captchaReceiver.Focus();
        }

        private void captchaReceiver_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                alerts.Content = "Aguarde... Checando Captcha";
                this.IsEnabled = false;
                this.CaptchaPostData["text"] = captchaReceiver.Text;
                Utils.HTTP http = new Utils.HTTP()
                {
                    CookieJar = Session.CookieJar,
                    Method = Utils.HttpRequestMethods.POST
                }.BindHttpRequestComplete((str) =>
                {
                    if (str.Contains("Turing inv"))
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            this.IsEnabled = true;
                            captchaReceiver.Text = string.Empty;
                            CaptchaLoaded = false;
                            captchaReceiver.Focus();
                            alerts.Content = "Captcha Incorreto!";
                        }));
                    }
                    else if (str.Contains("Nick inv"))
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            this.IsEnabled = true;
                            captchaReceiver.Text = string.Empty;
                            CaptchaLoaded = false;
                            captchaReceiver.Focus();
                            alerts.Content = "Nick Incorreto!";
                        }));
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            Session.InUse = true;
                            Bot.Settings.SessionsInUse.Add(Session);
                            Session.Wrapper.ReloadSessions();
                            CQ cq = new CQ(str);
                            this.ListemURL = cq.Find("#frame_view_panel").First().Attr("src");
                            CheckRoomCode += new Sessions.CheckRoomCode(CheckingRoomCode);
                            Session.BotStatus = BotStatus.Starting;
                            BeginListem();
                        }));
                    }
                }).RequestAsync("http://bp.batepapo.uol.com.br/room.html", this.CaptchaPostData);
            }
        }

        private void imageHolder_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (CaptchaLoaded)
            {
                CaptchaLoaded = false;
            }
            captchaReceiver.Focus();
        }

        private void CheckingRoomCode(string HTML)
        {
            SendQueryString = String.Empty;
            if (HTML.Contains("ro="))
            {
                Regex regex = new Regex(@"""ro=(.*?)""");
                MatchCollection matches = regex.Matches(HTML);
                if (matches.Count > 0)
                {
                    if (matches[0].Groups.Count > 1)
                    {
                        SendQueryString = matches[0].Groups[1].Value;
                        this.SendURL = "http://bp.batepapo.uol.com.br/send.html?ro=" + SendQueryString.Substring(0, SendQueryString.Length - 14) + "&nout=1";
                    }
                }
                Session.BotStatus = BotStatus.Shotting;
                BeginSending();
                BeginSendingPrivate();
                CheckRoomCode = null;
                CheckUsersOnline += new Sessions.CheckUsersOnline(CheckingUsersOnline);
                Utils.Logger.Write(String.Format("Sessão {0} iniciada com sucesso", Session.Name));
            }
            else if (HTML.Contains("Ocorreu um erro") && HTML.Contains("acentos dispon"))
            {
                CheckRoomCode = null;
                Utils.Logger.Write(String.Format("Sessão {0} esta com a sala cheia", Session.Name));
                Kill();
            }
        }

        private void CheckingUsersOnline(string HTML)
        {
            MatchCollection matches = new Regex(@"Load_Combo\(""re"", ""(.*)"", 1\)\;").Matches(HTML);
            if (matches.Count > 0)
            {
                if (matches[matches.Count - 1].Groups.Count > 1)
                {
                    string users = matches[matches.Count - 1].Groups[1].Value;
                    Session.OnlineUsers = users.Split('>');
                }
            }
        }
        public Utils.HTTP ListemHandler;
        private void BeginListem()
        {
            ListemHandler = new Utils.HTTP()
            {
                CookieJar = Session.CookieJar
            }.Async(
                ListemURL,
                new Utils.AsyncProgressDelegate((s) =>
                {
                    s.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    Session.HTML = s.Content;
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (CheckRoomCode != null)
                        {
                            CheckRoomCode(Session.HTML);
                        }
                        if (CheckUsersOnline != null)
                        {
                            CheckUsersOnline(Session.HTML);
                        }
                    }));
                }));
            Session.Kill = new Action(() =>
            {
                ListemHandler.KillAsync();
                SendMessage(".");
            });
        }

        private void BeginSending()
        {
            SendThread = new Thread(() =>
            {

                Bot.Session Session = this.Session;
                List<string> oldList = new List<string>();
                List<string> randomUsedMessages = new List<string>();

                Utils.Logger.Write(String.Format("Disparos publicos na sessão \"{0}\" iniciarão em {1} segundos", Session.Name, (Bot.Settings.MoreSettings.SendMessageInterval).ToString()));

                foreach (Int32 e in new int[(Int32)Bot.Settings.MoreSettings.SendMessageInterval * 10])
                {
                    if (!Session.InUse)
                    {
                        return;
                    }
                    Thread.Sleep(100);
                }


                string message = string.Empty;


                while (true)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Session = this.Session;
                    }));
                    if (Session.BotStatus != BotStatus.Paused)
                    {
                        if (!Session.InUse)
                        {
                            return;
                        }
                        List<string> messages = Session.Messages.Split(';').ToList<string>().Where(e => e.Length > 0).ToList();
                        if (messages.Except(oldList).Concat(oldList.Except(messages)).Count() > 0)
                        {
                            oldList = messages;
                            randomUsedMessages = new List<string>();
                        }
                        if (Session.Method == "random")
                        {
                            message = messages[new Random().Next(messages.Count)];
                        }
                        else if (Session.Method == "random_but_unique")
                        {
                            if (randomUsedMessages.Count == messages.Count)
                            {
                                randomUsedMessages = new List<string>();
                            }

                            message = messages[new Random().Next(messages.Count)];

                            while (randomUsedMessages.Contains(message))
                            {
                                message = messages[new Random().Next(messages.Count)];
                            }

                            randomUsedMessages.Add(message);
                        }

                        foreach (var i in (from u in Session.OnlineUsers where u != Session.Nickname select u).OrderBy(u => Guid.NewGuid()).ToList())
                        {
                            message = message.Replace("{nome}", i);
                            Utils.Logger.Write(String.Format("Sessão \"{0}\" disparou \"{1}\", o próximo disparo será em {2} segundos", Session.Name, message, (Bot.Settings.MoreSettings.SendMessageInterval).ToString()));
                            SendMessage(message, i);
                            break;
                        }
                    }

                    foreach (Int32 e in new int[(Int32)Bot.Settings.MoreSettings.SendMessageInterval * 10])
                    {
                        if (!Session.InUse)
                        {
                            return;
                        }
                        Thread.Sleep(100);
                    }

                }
            })
            {
                Name = String.Format("Message Sender {0} {1}", this.Session.Name, DateTime.Now)
            };
            SendThread.Start();
        }
        private void BeginSendingPrivate()
        {
            SendPrivateThread = new Thread(() =>
            {
                Bot.Session Session = this.Session;
                List<string> oldList = new List<string>();
                List<string> randomUsedMessages = new List<string>();
                string message = string.Empty;

                Utils.Logger.Write(String.Format("Disparos privados na sessão \"{0}\" iniciarão em {1} segundos", Session.Name, (Bot.Settings.MoreSettings.SendPrivateMessageInterval).ToString()));

                foreach(Int32 e in new int[(Int32)Bot.Settings.MoreSettings.SendPrivateMessageInterval*10]){
                    if (!Session.InUse)
                    {
                        return;
                    }
                    Thread.Sleep(100);
                }

                while (true)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Session = this.Session;
                    }));
                    if (Session.BotStatus != BotStatus.Paused)
                    {
                        if (!Session.InUse)
                        {
                            return;
                        }
                        List<string> messages = Session.PvMessages.Split(';').ToList<string>().Where(e => e.Length > 0).ToList();
                        if (messages.Except(oldList).Concat(oldList.Except(messages)).Count() > 0)
                        {
                            oldList = messages;
                            randomUsedMessages = new List<string>();
                        }
                        if (Session.Method == "random")
                        {
                            message = messages[new Random().Next(messages.Count)];
                        }
                        else if (Session.Method == "random_but_unique")
                        {
                            if (randomUsedMessages.Count == messages.Count)
                            {
                                randomUsedMessages = new List<string>();
                            }

                            message = messages[new Random().Next(messages.Count)];

                            while (randomUsedMessages.Contains(message))
                            {
                                message = messages[new Random().Next(messages.Count)];
                            }

                            randomUsedMessages.Add(message);
                        }
                        if (Session.OnlineUsers.Count() > 1)
                        {
                            foreach (var user in Session.OnlineUsers)
                            {
                                message = message.Replace("{nome}", user);
                                SendMessage(message, user, true);
                            }
                            Utils.Logger.Write(String.Format("Sessão \"{0}\" disparou \"{1}\" para os usuários <{2}>, o próximo disparo será em {3} segundos", Session.Name, message, String.Join(", ", Session.OnlineUsers), (Bot.Settings.MoreSettings.SendMessageInterval).ToString()));
                        }
                    }
                    foreach (Int32 e in new int[(Int32)Bot.Settings.MoreSettings.SendPrivateMessageInterval * 10])
                    {
                        if (!Session.InUse)
                        {
                            return;
                        }
                        Thread.Sleep(100);
                    }
                }
            })
            {
                Name = String.Format("Message Private Sender {0} {1}", this.Session.Name, DateTime.Now)
            };
            SendPrivateThread.Start();
        }

        private void SendMessage(string text, string to = null, bool isPrivate = false)
        {
            Bot.Settings.MessageQueue.Enqueue(new Utils.TQueueObject()
            {
                Thread = new Thread((qobj) =>
                {
                    TQueueObject t = (TQueueObject)qobj;
                    string[] hiddenChars = new string[] { "&nbsp;", "&nbsp;&nbsp;", "&nbsp;&nbsp;&nbsp;", "&nbsp;&nbsp;&nbsp;&nbsp;", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" };
                    Random random = new Random();
                    string hiddenChar = hiddenChars[random.Next(0, hiddenChars.Length - 1)];

                    Dictionary<string, string> post = new Dictionary<string, string>() { 
                        {"so","NULL"},
                        {"ei","NULL"},
                        {"pk",""},
                        {"re",""},
                        {"st","fala para"},
                        {"me",text+hiddenChar},
                        {"x","0"},
                        {"y","0"}
                    };

                    if (to != null)
                    {
                        post["re"] = to;
                    }

                    if (isPrivate)
                    {
                        post["ty"] = "false";
                    }

                    new Utils.HTTP()
                    {
                        Method = Utils.HttpRequestMethods.POST,
                        CookieJar = Session.CookieJar,
                        PostEncoding = Encoding.GetEncoding("ISO-8859-1")
                    }.BindHttpRequestComplete(new HttpRequestComplete((e) =>
                    {
                        t.TQueue.Next();
                    })).RequestAsync(SendURL, post);
                })
            });

            Bot.Settings.MessageQueue.Play();
        }
        private void Kill()
        {
            if (Session.Kill != null)
            {
                Session.Kill.Invoke();
                Session.Kill = null;
                Session.BotStatus = Bot.BotStatus.Waiting;
            }
            if (SendThread != null)
            {
                SendThread.Abort();
            }
            if (SendPrivateThread != null)
            {
                SendPrivateThread.Abort();
            }
            Session.InUse = false;
            Session.Wrapper.ReloadSessions();
            Bot.Settings.SessionsInUse.Remove(Session);
        }
    }
}
