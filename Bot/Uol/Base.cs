﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot.Uol
{
    class Base : IBase, INotifyPropertyChanged
    {
        public static DB.UolDB SettingsContext = new DB.UolDB();
        public static Themes Themes;
        public static Dictionary<string, string> Colors = new Dictionary<string, string>
        {
            {"#FFFC01","Amarelo"},
            {"#FEBA00","Caramelo"},
            {"#FE5B00","Laranja"},
            {"#FE0000","Vermelho"},
            {"#FE00BA","Rosa"},
            {"#AE00FF","Lilas"},
            {"#1E00FF","Azul Marinho"},
            {"#00C6FF","Azul Piscina"},
            {"#3CFF00","Verde limão"},
            {"#000000","Preto"}
        };

        public Base()
        {
            Themes = new Themes();
            string login = new Utils.HTTP()
            {
                Method = Utils.HttpRequestMethods.POST,
                CookieJar = CookieJar
            }.SimpleRequest("https://acesso.uol.com.br/login.html", new Dictionary<string, string>()
            {
                {"user","wesley.schleumer"},
                {"pass","will46"},
                {"skin",""},
                {"dest",""}
            });
            Logged = true;
            UseProfile = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        private System.Net.CookieContainer _cookieJar = new System.Net.CookieContainer();
        public System.Net.CookieContainer CookieJar
        {
            get { return _cookieJar; }
            set { _cookieJar = value; }
        }

        private bool _logged = false;
        public bool Logged
        {
            get { return _logged; }
            set { _logged = value; }
        }

        private bool _useProfile;

        public bool UseProfile
        {
            get { return _useProfile; }
            set { _useProfile = value; }
        }
        
    }
}
