﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot.Uol
{
    class Themes : BindingList<Theme>
    {
        public Themes()
        {
            (from item in Bot.Uol.Base.SettingsContext.Themes select item).ToList().ForEach(item => {
                base.Add(new Theme()
                {
                    Id = item.id,
                    Name = item.name,
                    Url = item.url
                });
            });
        }
    }
}
