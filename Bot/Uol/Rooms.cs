﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot.Uol
{
    class Rooms : BindingList<Room>
    {
        public Rooms(string theme_id)
        {
            Int32 tid = System.Convert.ToInt32(theme_id);
            (
                from item in Bot.Uol.Base.SettingsContext.Rooms
                where item.theme_id == tid
                select item).ToList().ForEach(item => {
                    Room room = new Room();
                    room.Id = item.id;
                    room.Name = item.name;
                    room.Url = item.url;
                    room.ThemeId = item.theme_id;
                    base.Add(room);
            });
        }
    }
}
