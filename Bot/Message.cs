﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot
{
    public class Message : INotifyPropertyChanged
    {
        private long? _id = 0;

        public long? Id
        {
            get { return _id; }
            set { _id = value; NotifyPropertyChanged("Id"); }
        }

        private string _text = String.Empty;

        public string Text
        {
            get { return _text; }
            set { _text = value; NotifyPropertyChanged("Text"); }
        }

        private long? _sessionId = 0;

        public long? SessionId
        {
            get { return _sessionId; }
            set { _sessionId = value; NotifyPropertyChanged("SessionId"); }
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
