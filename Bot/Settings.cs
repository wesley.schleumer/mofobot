﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.ComponentModel;

namespace MofoBot.Bot
{
    class Settings 
    {
        public static MofoBot.DB Context = new MofoBot.DB();
        public static Dictionary<string, MofoBot.Bot.IBase> Bases = new Dictionary<string, Bot.IBase>();
        public static Bot.Sessions Sessions;
        public static BindingList<Session> SessionsInUse = new BindingList<Session>();
        public static Bot.Nicknames Nicknames;
        public static string TempDir = System.IO.Directory.GetCurrentDirectory() + "\\tmp\\";

        public static MoreSettings MoreSettings = new MoreSettings();

        public static Utils.TQueue MessageQueue = new Utils.TQueue();

        private int SendPrivate;

        public int MyProperty
        {
            get { return SendPrivate; }
            set { SendPrivate = value; }
        }
        

        public static void LoadBases()
        {
            if (System.IO.Directory.Exists(TempDir))
            {
                System.IO.Directory.Delete(TempDir,true);
            }
            System.IO.Directory.CreateDirectory(TempDir);

            var q = from t in Assembly.GetExecutingAssembly().GetTypes()
                    select t;
            q.ToList().ForEach(t =>
            {
                Regex regexp = new Regex(@"MofoBot\.Bot\.([A-Za-z]+)$");
                MatchCollection matches = regexp.Matches(t.Namespace);
                if (matches.Count > 0)
                {
                    string Base = t.Namespace + ".Base";
                    try
                    {
                        string nBase = matches[0].Groups[1].Value;
                        if (!Bases.Keys.Contains(nBase))
                        {
                            Type type = Type.GetType(Base);
                            MofoBot.Bot.IBase instance = (MofoBot.Bot.IBase)Activator.CreateInstance(type);
                            Bases.Add(nBase, instance);
                            Console.WriteLine(Base + " Loaded");
                        }
                    }
                    catch (Exception) { }
                }
            });
            Bot.Settings.Sessions = new Bot.Sessions();
            Bot.Settings.Nicknames = new Bot.Nicknames();
        }
    }
}
