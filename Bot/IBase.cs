﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MofoBot.Bot
{
    public interface IBase
    {

        System.Net.CookieContainer CookieJar
        {
            get;
            set;
        }

        bool Logged
        {
            get;
            set;    
        }

        bool UseProfile
        {
            get;
            set;
        }
        
    }
}
