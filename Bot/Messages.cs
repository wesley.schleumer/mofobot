﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot
{
    public class Messages : BindingList<Message>
    {
        public long SessionId = 0;
        public Messages(bool IsPivot = false)
        {
            if (!IsPivot)
            {
                (from d in Bot.Settings.Context.Messages group d by d.text into grouping select grouping).ToList().ForEach(d =>
                {
                    var e = d.First();
                    base.Add(new Message()
                    {
                        Id = e.id,
                        Text = e.text,
                        SessionId = e.session_id
                    });
                });
            }
        }
        public Messages(long sessionID)
        {
            this.SessionId = sessionID;
            (from d in Bot.Settings.Context.Messages where d.session_id == sessionID select d).ToList().ForEach(d =>
            {
                base.Add(new Message()
                {
                    Id = d.id,
                    Text = d.text,
                    SessionId = d.session_id
                });
            });
        }

        new public void Add(Bot.Message message)
        {
            Console.WriteLine("LOL");
            if (message.Id == 0)
            {
                MofoBot.Message mess = new MofoBot.Message();
                mess.text = message.Text;
                Bot.Settings.Context.Messages.AddObject(mess);
                Bot.Settings.Context.SaveChanges();
                message.Id = mess.id;
            }
            base.Add(message);
        }

        new public void Remove(Bot.Message message)
        {
            if (message.Id != 0)
            {
                (from d in Bot.Settings.Context.Messages where d.text.ToUpper().Contains(message.Text.ToUpper()) select d).ToList().ForEach(obj => {
                    Console.WriteLine("LO");
                    Bot.Settings.Context.Messages.DeleteObject(obj);
                    Bot.Settings.Context.SaveChanges();
                });
                
            }
            base.Remove(message);
        }

    }
}
