﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MofoBot.Bot.Bol
{
    class Base : IBase
    {
        public System.Net.CookieContainer CookieJar
        {
            get;
            set;
        }


        public bool Logged
        {
            get;
            set;
        }


        public bool UseProfile
        {
            get;
            set;
        }
    }
}
