﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot
{
    class Nicknames : BindingList<Nickname>
    {
        public Nicknames()
        {
            (from d in Bot.Settings.Context.Nicknames select d).ToList().ForEach(d =>
            {
                base.Add(new Bot.Nickname()
                {
                    Id = d.id,
                    Value = d.value
                });
            });
        }

        new public void Add(Nickname nickname)
        {
            MofoBot.Nickname nick = new MofoBot.Nickname();
            nick.value = nickname.Value;
            Bot.Settings.Context.Nicknames.AddObject(nick);
            Bot.Settings.Context.SaveChanges();
            nickname.Id = nick.id;
            base.Add(nickname);
        }
    }
}
