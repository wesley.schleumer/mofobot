﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.ComponentModel;
using System.Collections;

namespace MofoBot.Bot
{
    [Flags]
    public enum BotStatus
    {
        Waiting = 0,
        Starting = 1,
        Shotting = 2,
        Paused = 3
    }
    [Serializable()]
    public class Session : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private CookieContainer _cookieJar = new CookieContainer();

        public CookieContainer CookieJar
        {
            get { return _cookieJar; }
            set { _cookieJar = value; }
        }


        private long _id = 0;

        [Browsable(false)]
        public long Id
        {
            get { return _id; }
            set { _id = value; NotifyPropertyChanged("Id"); }
        }

        private string _name = String.Empty;

        [DisplayName("Nome")]
        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyPropertyChanged("Name"); }
        }

        private string _url = String.Empty;

        [DisplayName("Url da Sala")]
        public string Url
        {
            get { return _url; }
            set { _url = value; NotifyPropertyChanged("Url"); }
        }

        private string _nickname = String.Empty;

        [DisplayName("Nickname")]
        public string Nickname
        {
            get { return _nickname; }
            set { _nickname = value; NotifyPropertyChanged("Nickname"); }
        }

        private string _color = String.Empty;

        [DisplayName("Cor(Hexadecimal)")]
        public string Color
        {
            get { return _color; }
            set { _color = value; NotifyPropertyChanged("Color"); }
        }

        private IBase _base;

        [Browsable(false)]
        public IBase Base
        {
            get { return _base; }
            set { 
                _base = value;
                if (Base.CookieJar != null)
                {
                    this.CookieJar = Utils.ObjectCopier.Clone<CookieContainer>(Base.CookieJar);
                }
                NotifyPropertyChanged("Base"); 
            }
        }

        private string _baseName;

        [Browsable(false)]
        public string BaseName
        {
            get { return _baseName; }
            set { _baseName = value; NotifyPropertyChanged("BaseName"); }
        }

        private bool _autoStart;

        [Browsable(false)]
        public bool AutoStart
        {
            get { return _autoStart; }
            set { _autoStart = value; NotifyPropertyChanged("AutoStart"); }
        }

        private string _method;

        [DisplayName("Metodo de Envio")]
        public string Method
        {
            get { return _method; }
            set { _method = value; NotifyPropertyChanged("Method"); }
        }

        private string _messages;

        public string Messages
        {
            get { return _messages; }
            set
            {
                _messages = value;
                NotifyPropertyChanged("Messages");
            }
        }


        private string _pvMessages;

        public string PvMessages
        {
            get { return _pvMessages; }
            set
            {
                _pvMessages = value;
                NotifyPropertyChanged("PvMessages");
            }
        }

        private bool _inUse = false;

        public bool InUse
        {
            get { return _inUse; }
            set { _inUse = value; NotifyPropertyChanged("InUse"); }
        }

        private Elements.ShotSessions _wrapper;

        public Elements.ShotSessions Wrapper
        {
            get { return _wrapper; }
            set { _wrapper = value; NotifyPropertyChanged("Wrapper"); }
        }

        private bool _focusIt = false;

        public bool FocusIt
        {
            get { return _focusIt; }
            set { _focusIt = value; NotifyPropertyChanged("FocusIt"); }
        }

        private string _HTML = String.Empty;

        public string HTML
        {
            get { return _HTML; }
            set { _HTML = value; NotifyPropertyChanged("HTML"); }
        }

        private string[] _onlineUsers = new string[] { };

        public string[] OnlineUsers
        {
            get { return _onlineUsers; }
            set { _onlineUsers = value; NotifyPropertyChanged("OnlineUsers"); }
        }

        private BotStatus _botStatus = BotStatus.Waiting;

        public BotStatus BotStatus
        {
            get { return _botStatus; }
            set { _botStatus = value; NotifyPropertyChanged("BotStatus"); }
        }


        public Action Kill = null;

        public Session()
        {
            
        }


        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public bool Validate()
        {
            if (Name.Length < 1)
            {
                return false;
            }
            if (Nickname.Length < 4)
            {
                return false;
            }
            if (!Bot.Uol.Base.Colors.Keys.Contains(Color))
            {
                return false;
            }

            return true;
        }

        public void Save()
        {
            var sess = (from p in Bot.Settings.Context.Sessions where p.id == this.Id select p).Single();
            sess.messages = Messages;
            sess.method = Method;
            sess.pvmessages = PvMessages;
            sess.color = Color;
            sess.name = Name;
            sess.url = Url;
            sess.nickname = Nickname;
            Bot.Settings.Context.SaveChanges();
        }
    }
}
