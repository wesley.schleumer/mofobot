﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MofoBot.Bot
{
    class Sessions : BindingList<Session>
    {
        public Sessions()
        {
            (from d in Bot.Settings.Context.Sessions select d).ToList().ForEach(d =>
            {
                Bot.IBase Base = null;
                try
                {
                    Base = Settings.Bases[d.@base];
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    base.Add(new Bot.Session()
                    {
                        Id = d.id,
                        Name = d.name,
                        Url = d.url,
                        Nickname = d.nickname,
                        Color = d.color,
                        AutoStart = System.Convert.ToBoolean(d.autostart),
                        PvMessages = d.pvmessages != null ? d.pvmessages : String.Empty,
                        Messages = d.messages != null ? d.messages : String.Empty,
                        Method = d.method,
                        Base = Base,
                        BaseName = d.@base
                    });
                }
            });
        }
        new public void Add(Bot.Session Session)
        {
            if (Session.Id == 0)
            {
                MofoBot.Session sess = new MofoBot.Session();
                sess.name = Session.Name;
                sess.url = Session.Url;
                sess.nickname = Session.Nickname;
                sess.color = Session.Color;
                sess.autostart = System.Convert.ToInt32(Session.AutoStart);
                sess.method = Session.Method;
                sess.messages = Session.Messages;
                sess.pvmessages = Session.PvMessages;
                sess.@base = Session.BaseName;
                Bot.Settings.Context.Sessions.AddObject(sess);
                Bot.Settings.Context.SaveChanges();
                Session.Id = sess.id;
                Session.Base = Settings.Bases[Session.BaseName];
            }
            base.Add(Session);
        }

        new public void Remove(Bot.Session Session)
        {
            var sess = (from s in Bot.Settings.Context.Sessions where s.id == Session.Id select s).Single();
            Bot.Settings.Context.Sessions.DeleteObject(sess);
            Bot.Settings.Context.SaveChanges();
            if (Session.Kill != null)
            {
                Session.Kill.Invoke();
            }
            if (Session.InUse)
            {
                Bot.Settings.SessionsInUse.Remove(Session);
            }
            base.Remove(Session);
        }
    }
}
