﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MofoBot.Bot
{
    public class MoreSettings : INotifyPropertyChanged
    {
        private System.Nullable<double> _sendMessageInterval = 30;

        public System.Nullable<double> SendMessageInterval
        {
            get { return _sendMessageInterval; }
            set { _sendMessageInterval = value; NotifyPropertyChanged("SendMessageInterval"); }
        }

        private System.Nullable<double> _sendPrivateMessageInterval = 300;

        public System.Nullable<double> SendPrivateMessageInterval
        {
            get { return _sendPrivateMessageInterval; }
            set { _sendPrivateMessageInterval = value; NotifyPropertyChanged("SendPrivateMessageInterval"); }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
